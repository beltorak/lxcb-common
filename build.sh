#!/bin/bash

#__COMMON__

set -e
set -u

_at_exit () {
	echo "$*" >> "$T/at-exit"
}
_exit () {
	cat "$T/at-exit" | nl | sort -gr | sed -e 's/^[ \t]*[0-9]*[ \t]*//' > "$T/at-exit-script"
	. "$T/at-exit-script"
}
trap '_exit' 0

T="$(mktemp -d -t "tmp.${SCRIPT_NAME}.XXXXXXXX")"
_at_exit rm -rf "$T"

# An arbitrary function return value
declare -a _ret

section_raw () {
	local file="$(readlink -f "$1")"
	shift

	local HEAD
	for HEAD in "$@"; do
		if [ "${HEAD}" = '-' ]; then
			HEAD=1
		else
			HEAD="/^#__${HEAD}__/"
		fi
		cat "$file" | sed -n -e "${HEAD},/^#__/ { /^#__/d ; p }"
	done
}

section () {
	section_raw "$@" | sed -n -e '/^#/ d ; /^\s*$/ d ; p'
}

banner () { (
	set +x
	sym="$1"
	shift
	printf '%040d\n' 0 | tr 0 "$sym"
	printf "%s\n%s %s\n%s\n" "$sym" "$sym" "$*" "$sym"
	printf '%040d\n' 0 | tr 0 "$sym"
) }

parse_sources_line () {
	local method="$1"
	shift
	local download_dir="$1"
	shift

	local uri="${*%% *}"
	local tags="${*#* }"
	
	if [ "$uri" = "$tags" ]; then
		tags="[]"
	fi
	
	if [ "${tags:0:1}${tags:$((${#tags}-1))}" != "[]" ]; then
		echo "Sources Line Malformed: $*" >&2
		return 1
	fi
	
	local -A temp_sources
	local -a tagnames
	local filename name
	local tagname tagval
	while read tagname tagval; do
		case $tagname in
			"")
				continue
			;;
			name)
				name=$tagval
			;;
			filename)
				filename=$tagval
			;;
			*)
				tagnames+=($tagname)
				printf -v temp_sources[${tagname}] "%s" "$tagval"
			;;
		esac
	done < <(echo "$tags" | sed -e 's/^\[// ; s/\]$// ; s/ \+/\n/g ; s/=/ /g')

	if [ -n "${tagnames:-}" ]; then
		for tagname in "${tagnames[@]}"; do
			uri="$(echo "$uri" | sed -e "s/<$tagname>/${temp_sources[${tagname}]}/g")"
		done
	fi
	if [ -z "${filename:-}" ]; then
		filename="$(basename "$(dirname "$uri/x")")"
	fi
	if [ -z "${name:-}" ]; then
		name="${filename%%.*}"
	fi
	if [ -n "${tagnames:-}" ]; then
		for tagname in "${tagnames[@]}"; do
			printf -v sources[${name}.${tagname}] "%s" "${temp_sources[${tagname}]}"
		done
		printf -v sources[${name}._tag_names] "%s" "${tagnames[@]}"
	fi

	printf -v sources[${name}._download_method] "%s" "$method"
	printf -v sources[${name}.filename] "%s" "$filename"
	printf -v sources[${name}.filepath] "%s" "$download_dir/$filename"
	printf -v sources[${name}.uri] "%s" "$uri"

	printf -v sources[_names] "%s %s" "${sources[_names]:-}" "$name"

	_retval="$name"
}

parse_version () {
	local name="$1"
	case "${sources[${name}.version]}" in
		"@date")
			case "${sources[${name}._download_method]}" in
				git)
					( cd "${sources[${name}.filepath]}"; env TZ=GMT date -d "$(git log --date=iso -1 --pretty=format:%cd)" +%Y%m%d.%H%M%S )
				;;
				*)
					echo "${sources[${name}.version]}"
				;;
			esac
		;;
		*)
			echo "$2"
		;;
	esac
}

do_download () {
	local name="$1"

	mkdir -p "$(dirname "${sources[${name}.filepath]}")"
	case "${sources[${name}._download_method]}" in
		http)
			[[ -r "${sources[${name}.filepath]}" ]] && break;
			wget -q -O "${sources[${name}.filepath]}" "${sources[${name}.uri]}"
		;;
		git)
			[[ -r "${sources[${name}.filepath]}" ]] && break;
			git clone "${sources[${name}.uri]}" "${sources[${name}.filepath]}"
		;;
		hg)
			[[ -r "${sources[${name}.filepath]}" ]] && break;
			hg clone "${sources[${name}.uri]}" "${sources[${name}.filepath]}"
		;;
		zip)
			(
				source_path="$(readlink -f "${sources[${name}.uri]}")"
				rm -vf "${sources[${name}.filepath]}"
				if [ -d "$source_path" ]; then
					cd "$source_path"
					zip -r "${sources[${name}.filepath]}" .
				else
					cd "$(dirname "$source_path")"
					zip -r "${sources[${name}.filepath]}" "$(basename "$source_path")"
				fi
			)
		;;
		*)
			echo "Bad Download Method for $name (${sources[${name}.uri]})" >&2
			return 1
		;;
	esac
}

vc_update () {
	local name="$1"

	case "${sources[${name}._download_method]}" in
		git)
			(
				cd "${sources[${name}.filepath]}"
				if [ -n "${sources[${name}.switch-to]:-}" ]; then
					echo "    Switching to $name @ ${sources[${name}.switch-to]}"
					git checkout -q "${sources[${name}.switch-to]}"
				fi
				if [ -r .gitmodules ]; then
					git submodule init && git submodule update
				fi
			)
		;;
		hg)
			(
				cd "${sources[${name}.filepath]}"
				if [ -n "${sources[${name}.switch-to]:-}" ]; then
					echo "Switching to $name @ ${sources[${name}.switch-to]}"
					hg checkout "${sources[${name}.switch-to]}"
				fi
			)
		;;
		*)
		;;
	esac
}

declare -A sources
parse_sources () {
	banner + "Parsing Sources"

	local download_dir="$SCRIPT_HOME/cache"
	mkdir -p "$download_dir"

	local method
	local -a filenames
	for method in ZIP HTTP GIT HG; do
		local line
		while read line; do
			parse_sources_line ${method,,} "$download_dir" "$line"
		done < <(section "$SCRIPT_HOME/sources" $method)
	done
}

get_sources () {
	parse_sources
	local download_dir="$SCRIPT_HOME/cache"

	banner + "Getting Sources"

	local name
	local -a filenames
	for name in ${sources[_names]}; do
		echo "[${sources[${name}._download_method]}] ${sources[${name}.uri]} -> ${sources[${name}.filename]}"
		do_download "$name"
		vc_update "$name"
		filenames+=( ${sources[${name}.filename]} )
	done

	(/bin/ls "$download_dir"; echo ${filenames[@]}) | tr ' ' '\n' | sort | uniq -u \
	| while read line; do
		echo "Removing cached '$line' (not in 'sources')"
		rm -rf "$download_dir/$line"
	done
}

do_install_packages () {
	local packages=($(section "$SCRIPT_HOME/sources" APT_GET | tr '\n' ' '))
	if [ -n "${packages:-}" ]; then
		sudo apt-get install -y "${packages[@]}"
	fi
}

install_packages () {
	banner + "Installing Packages"
	sudo apt-get update
	do_install_packages
}

#__HOST__

cat_help () {
	cat<<__EOF__ | sed -e 's/^\t\t//'
		usage: $SCRIPT_NAME --get-sources
		       $SCRIPT_NAME --prep-lxc [prep-lxc-opts] lxc-container lxc-username [ssh-key]
		       $SCRIPT_NAME lxc-container lxc-username [ssh-key]
		
		The third form is used for building on a prepared LXC machine.
		The second form is used to prepare an LXC machine.
		The first form is to download all the sources, which can speed up
		    repeated LXC builds.
		
		Arguments:
		    lxc-container       The name of the LXC container to start.
		                        For building, this is the base name of a new
		                        ephemeral machine.
		                        For preparing an LXC, this is the machine to
		                        prepare.
		    lxc-username        The name of the user to log into the LXC with.
		    ssh-key             The SSH key to use to log into the machine.
		                        Defaults to the same name as the machine.
		                        See SSH-KEY SEARCH PATHS.
		
		--get-sources           downloads sources to speed up repeated lxc builds
		--prep-lxc              Prepare an lxc machine to be used for builds.
		    Options:
		        --packages      Update system packages and install those in 'sources'.
		        --setup-sudo    Prepare the machine for unrestricted SUDO as the login user.
		        -o base-machine Clone a new machine from this base machine.
		
		SSH-KEY SEARCH KEY_PATHS
		The following paths are searched for the ssh-key:
		$(for path in "${KEY_SEARCH_PATHS[@]}"; do echo " - $path"; done | sed -e "s;$SCRIPT_HOME\(/\)\?;"'$PWD\1;')
		
__EOF__
}

parse_args () {
	local -n opt_ref="$1"
	shift
	local args=( "$@" )
	
	local default
	for default in help-requested get-sources prep-lxc; do
		printf -v ${!opt_ref}[$default] "%s" 0
	done
	local i
	_ret=()
	local slurp_args=0
	for (( i=0; i<${#args[@]}; i++ )); do
		if ((slurp_args)); then
			_ret+=( "${args[@]:$i}" )
			break
		else
			case "${args[$i]}" in
				--)
					slurp_args=1
				;;
				--help)
					printf -v ${!opt_ref}[help-requested] "%s" 1
					return 0
				;;
				--get-sources)
					if (( i > 0 )); then
						echo "Option '--get-sources' must come first if present." >&2
						return 1
					fi
					printf -v ${!opt_ref}[get-sources] "%s" 1
					break
				;;
				--prep-lxc)
					if (( i > 0 )); then
						echo "Option '--prep-lxc' must come first if present." >&2
						return 1
					fi
					printf -v ${!opt_ref}[prep-lxc] "%s" 1
					slurp_args=1
				;;
				-*)
					echo "Bad Argument: ${args[$i]}" >&2
					return 1
				;;
				*)
					_ret+=( "${args[$i]}" )
				;;
			esac
		fi
	done
}

parse_args_for_lxc_prep () {
	local -n opt_ref="$1"
	shift
	local args=( "$@" )
	
	_ret=()
	
	printf -v ${!opt_ref}[install-packages] "%s" 0
	
	local i
	local slurp_args=0
	for (( i=0; i<${#args[@]}; i++ )); do
		if ((slurp_args)); then
			_ret+=( "${args[@]:$i}" )
			break
		else
			case "${args[$i]}" in
				-o)
					i=$((i+1))
					printf -v ${!opt_ref}[base-machine] "%s" "${args[$i]}"
				;;
				--packages)
					printf -v ${!opt_ref}[install-packages] "%s" 1
				;;
				--setup-sudo)
					printf -v ${!opt_ref}[unrestricted-sudo] "%s" 1
				;;
				-*)
					echo "Bad Argument: ${args[$i]}" >&2
					return 1
				;;
				*)
					_ret+=( "${args[$i]}" )
				;;
			esac
		fi
	done
}

prep_lxc () {
	local -A lxc
	parse_args_for_lxc_prep lxc "$@"
	local -A remote
	parse_remoting_args remote ${_ret[*]:+"${_ret[@]}"}
	
	remote[key-path]="$(print_ssh_key_path "${remote[key-name]}")"
	
	if [ -z "${remote[key-path]:-}" ]; then
		remote[key-path]="${KEY_SEARCH_PATHS}/${remote[key-name]}"
		banner + "Generating key ${remote[key-path]}"
		mkdir -p "$(dirname "${remote[key-path]}" )"
		ssh-keygen -f "${remote[key-path]}" -q -N ""
	fi
	
	if [ -n "${lxc[base-machine]:-}" ]; then
		banner + "Cloning ${lxc[base-machine]} to ${remote[machine]}"
		sudo lxc-clone -o "${lxc[base-machine]}" "${remote[machine]}"
	fi
	
	banner + "Starting ${remote[machine]}"
	sudo lxc-start -d -n "${remote[machine]}"
	_at_exit "shutdown_machine ${remote[machine]}"
	set_ipv4 remote
	echo " ... got IP: ${remote[ipv4]}"
	
	local ssh_opts=( -i "${remote[key-path]}" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null )
	create_temp_script login ssh "${ssh_opts[@]}" -q "${remote[user-name]}@${remote[ipv4]}"
	create_temp_script copy scp "${ssh_opts[@]}" -q
	banner - "Copying key to ${remote[machine]}"
	ssh-copy-id "${ssh_opts[@]}" "${remote[user-name]}@${remote[ipv4]}"
	
	if ((lxc[unrestricted-sudo])); then
		"$T/login" "sudo /bin/true" && {
			echo "SUDO apparently already unrestricted...."
		} || {
			banner - "Preparing Unrestricted SUDO"
			cat<<-"__EOF__" | "$T/login" "cat ->./unrestricted-sudo; chmod +x ./unrestricted-sudo"
					#!/bin/bash
					set -x
					echo "$(id -un) ALL=(ALL:ALL) NOPASSWD: ALL" \
					| sudo tee /etc/sudoers.d/99-unrestricted-"$(id -un)"
			__EOF__
			banner + "Run: ./unrestricted-sudo && logout"
			"$T/login"
			
			banner - "Testing SUDO"
			"$T/login" "sudo id && rm -f unrestricted-sudo" \
					&& { banner '!' 'SUCCESS'; } \
					|| { banner '!' 'FAIL!!1!'; return 1; }
		}
	fi
	
	if ((lxc[install-packages])); then
		banner + "Updating packages on ${remote[machine]}"
		"$T/copy" "$SCRIPT_HOME/sources" "${remote[user-name]}@${remote[ipv4]}:~"
		cat<<-__EOF__ | "$T/login" "cat -> build.sh; chmod +x ./build.sh; ./build.sh && rm -f build.sh sources"
			$(section_raw "$SCRIPT_SELF" -)
			$(section_raw "$BASH_SOURCE" COMMON)
			set -x
			sudo apt-get update
			sudo apt-get upgrade -y
			do_install_packages
		__EOF__
	fi
}

parse_remoting_args () {
	local varname="$1"
	shift
	printf -v ${varname}[machine] "%s" "${1:?Required: base LXC container name}"
	printf -v ${varname}[user-name] "%s" "${2:?Required: The LXC username to log in as}"
	printf -v ${varname}[key-name] "%s" "${3:-$1}"
}

KEY_SEARCH_PATHS=( "$SCRIPT_HOME/ssh-keys" "$SCRIPT_HOME/target" "$SCRIPT_HOME" "$HOME/.ssh/keys" )
print_ssh_key_path () {
	local key_name="$1"
	shift
	
	local key_dir
	for key_dir in "${KEY_SEARCH_PATHS[@]}"; do
		if [ -r "${key_dir}/${key_name}" -a "${key_dir}/${key_name}.pub" ]; then
			readlink -f "${key_dir}/${key_name}"
			break
		fi
	done
}

shutdown_machine () {
	local machine="$1"
	banner @ "To log in remotely, run $T/login"
	read -p "Enter to kill the machine..."
	sudo lxc-stop -n $machine
}

do_build () {
	local -n opts="$1"
	shift
	
	declare -A remote
	parse_remoting_args remote "$@"
	
	remote[key-path]="$(print_ssh_key_path "${remote[key-name]}")"
	if [ -z "${remote[key-path]:-}" ]; then
		echo "Bad Key; '${remote[key-name]}' not in SSH-KEY SEARCH PATHS" >&2
		exit 1
	fi

	remote[base-machine]="${remote[machine]}"
	remote[machine]="${remote[base-machine]}-$(hexdump -ve '/1 "%02x"' -n 4 /dev/urandom)"
	banner - "Starting ${remote[machine]}"
	sudo lxc-start-ephemeral -o ${remote[base-machine]} -n ${remote[machine]} -d
	_at_exit shutdown_machine ${remote[machine]}
	set_ipv4 remote
	
	local ssh_opts=( -i "${remote[key-path]}" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null )
	create_temp_script login ssh "${ssh_opts[@]}" -q "${remote[user-name]}@${remote[ipv4]}"
	create_temp_script copy scp "${ssh_opts[@]}" -q -r
	if [ -d "$SCRIPT_HOME/cache" ]; then
		"$T/copy" -r "$SCRIPT_HOME/cache" "${remote[user-name]}@${remote[ipv4]}:~"
	fi
	
	cat<<-__EOF__ | "$T/login" "cat -> build.sh"
		$(section_raw "$SCRIPT_SELF" -)
		$(section_raw "$BASH_SOURCE" COMMON)
		$(section_raw "$SCRIPT_SELF" REMOTE)
	__EOF__
	"$T/copy" "$SCRIPT_HOME/sources" "${remote[user-name]}@${remote[ipv4]}:~"
	
	banner "#" "Beginning Remote Build"
	rm -rf target; mkdir target
	"$T/login" "bash -x build.sh"
	banner "#" "Completed Remote Build"
	
	"$T/copy" "${remote[user-name]}@${remote[ipv4]}:~/built-file" "$T"
	"$T/copy" "${remote[user-name]}@${remote[ipv4]}:$(< "$T/built-file")" "$SCRIPT_HOME/target"
}

# 1: name of the assoc.array containing the name of the lxc container under key 'machine'
# result: assoc.array contains key 'ipv4'
set_ipv4 () {
	local -n ref_remote="$1"
	shift
	
	local ipv4=-
	local count=3
	while [ "$ipv4" = '-' ]; do
		if [ $count -gt 0 ]; then
			count=$(( -count ))
		elif [ $count -lt 0 ]; then
			echo "Waiting for IPv4 Address ($((-count*10)))..."
			sleep 10
			count=$(( count + 1))
		else
			echo "Gave up waiting for the LXC..." >&2
			exit 1
		fi
		{
			read
			read
			read ipv4
		} < <(sudo lxc-ls --fancy "^${ref_remote[machine]}\$" -F "ipv4")
	done
	
	printf -v ${!ref_remote}[ipv4] "%s" $ipv4
}

create_temp_script () {
	local script_name="$1"
	shift
	{
		echo '#!/bin/bash'
		printf "%q " "$@"
		echo '${1:+"$@"}'
	} > "$T/$script_name"
	chmod +x "$T/$script_name"
}

_main () {
	local -A opts
	parse_args opts "$@"
	local args=( ${_ret[*]:+"${_ret[@]}"} )

	if ((opts[help-requested])); then
		cat_help
		return 0
	fi

	if ((opts[get-sources])); then
		get_sources
		return 0
	fi
	
	if ((opts[prep-lxc])); then
		prep_lxc ${args[*]:+"${args[@]}"}
		return 0
	fi

	rm -rf "$SCRIPT_HOME/target"
	mkdir -p "$SCRIPT_HOME/target"
	{ do_build opts ${args[*]:+"${args[@]}"} 2>&1; } | tee "$SCRIPT_HOME/target/build.log"
}

_main "$@"

